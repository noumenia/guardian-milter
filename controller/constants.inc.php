<?php
/**
 * guardian-milter - Constants
 *
 * Define system-wide constants
 *
 * @copyright Noumenia (C) 2023 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage constants
 */

// Set application version
define("GUA_VER", "2.1-dev");

// Bank domains
define("BANK_DOMAINS", array(
	'AT' => array(
		"alpenprivatbank.com",
		"americanexpress.com",
		"awsg.at",
		"bankaustria.at",
		"gutmann.at",
		"krentschker.at",
		"bawagpsk.com",
		"erstegroup.com",
		"gecapital.com",
		"afrilandfirstbank.com",
		"oberbank.at",
		"rzb.at"
	),
	'BE' => array(
		"argenta.be",
		"attijariwafabank.com",
		"axabank.be",
		"aion.eu",
		"belfius.com",
		"beobank.be",
		"bnpparibasfortis.be",
		"crelan.be",
		"kbc.com",
		"keytradebank.be",
		"triodos.com",
		"vanlanschotkempen.com",
		"vdk.be"
	),
	'CH' => array(
		"ubs.com",
		"credit-suisse.com",
		"zkb.ch",
		"raiffeisen.ch",
		"postfinance.ch",
		"juliusbaer.com",
		"vontobel.com",
		"pictet.com",
		"lombardodier.com",
		"jsafrasarasin.com",
		"ubp.com",
		"efginternational.com"
	),
	'DE' => array(
		"bundesbank.de",
		"ecb.europa.eu",
		"bayernlb.de",
		"commerzbank.com",
		"consorsbank.de",
		"dab-bank.de",
		"dekabank.de",
		"db.com",
		"dzbank.com",
		"gls.de",
		"hcob-bank.de",
		"kfw.de",
		"lbbw.de",
		"lbb.de",
		"helaba.com",
		"national-bank.de",
		"fidor.de",
		"n26.com",
		"nordlb.com",
		"nrwbank.com",
		"solarisgroup.com",
		"wirecard.com"
	),
	'ES' => array(
		"santander.com",
		"bbva.com",
		"caixabank.com",
		"grupbancsabadell.com",
		"unicaja.es",
		"bankinter.com",
		"abanca.com",
		"kutxabank.com",
		"ibercaja.es"
	),
	'FR' => array(
		"rothschildandco.com",
		"group.bnpparibas",
		"bpce.fr",
		"banquepopulaire.fr",
		"caisse-epargne.fr",
		"palatine.fr",
		"cic.fr",
		"creditmutuel.com",
		"credit-agricole.com",
		"ca-cib.com",
		"creditlyonnais.fr",
		"credit-du-nord.fr",
		"hsbc.fr",
		"labanquepostale.com",
		"societegenerale.com"
	),
	'GR' => array(
		"nbg.gr",
		"eurobank.gr",
		"eurobank-estatements.gr",
		"alphabank.gr",
		"alpha.gr",
		"alphaecommerce.gr",
		"pancretabank.gr",
		"piraeusbank.gr",
		"winbank.gr",
		"atticabank.gr",
		"optimabank.gr"
	),
	'IT' => array(
		"intesasanpaolo.com",
		"unicreditgroup.eu",
		"bancaditalia.it",
		"cdp.it",
		"bancoposta.it",
		"creditosportivo.it",
		"bancobpm.it",
		"gruppomps.it",
		"bper.it",
		"bnl.it",
		"mediobanca.com",
		"credit-agricole.it",
		"bancamediolanum.it",
		"credem.it",
		"popso.it",
		"iccreabanca.it",
		"dexia-crediop.it",
		"popolarebari.it",
		"bancodesio.it",
		"sellagroup.eu",
		"bccroma.it",
		"bancacrasti.it"
	),
	'LU' => array(
		"advanzia.com",
		"banquedeluxembourg.com",
		"spuerkeess.lu",
		"bil.com",
		"kbc.com",
		"post.lu",
		"raiffeisen.lu",
		"vpbank.com"
	),
	'NL' => array(
		"dnb.nl",
		"fmo.nl",
		"abnamro.com",
		"ing.com",
		"rabobank.com",
		"snsbank.nl",
		"devolksbank.nl",
		"vanlanschotkempen.com",
		"binck.com"
	),
	'PT' => array(
		"bportugal.pt",
		"millenniumbcp.pt",
		"banif.pt",
		"bancobpi.pt",
		"santander.pt",
		"cgd.pt",
		"bancomontepio.pt",
		"novobanco.pt"
	)
));

