<?php
/**
 * guardian-milter - Common
 *
 * Common functionality
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage common
 */

// Initialize constants
require_once(dirname(__DIR__) . "/controller/constants.inc.php");

// Initialize the autoloader
require_once(dirname(__DIR__) . "/library/NoumeniaGuardianMilterAutoloader.inc.php");

// Register the autoloader
spl_autoload_register("NoumeniaGuardianMilterAutoloader", true);

// Set global configuration array with default values
Config::setDefaults();

// Initialize logging
require_once(dirname(__DIR__) . "/controller/loginit.inc.php");

// Load configuration
require_once(dirname(__DIR__) . "/controller/config.inc.php");

