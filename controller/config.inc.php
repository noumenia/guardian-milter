<?php
/**
 * guardian-milter - Configuration
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage config
 */

// Mailparse module, set character set to UTF-8
if(extension_loaded("mailparse"))
	ini_set("mailparse.def_charset", "UTF-8");

// INI configuration file
if(isset($cmdParameters['i']))
	$ini = $cmdParameters['i'];
elseif(isset($cmdParameters['ini']))
	$ini = $cmdParameters['ini'];
elseif(is_file("/etc/gu-milter.ini"))
	$ini = "/etc/gu-milter.ini";
else
	$ini = Config::readStr("ini");

// File check
if(!is_file($ini)) {

	Log::error("guardian-milter: ERROR - Failed to read the configuration file: " . $ini);
	exit(9);

}

// Parse INI file
$iniValues = parse_ini_file($ini, false, INI_SCANNER_TYPED);
if($iniValues === false) {

	Log::error("guardian-milter: ERROR - Failed to parse the configuration file: " . $ini);
	exit(9);

}

// Loop INI file option/value pairs
foreach($iniValues as $option => $value) {

	// Check for unknown option
	if(Config::isset($option) === false) {

		Log::error("guardian-milter: ERROR - Unknown INI option: " . strval($option));
		exit(9);

	}

	// Set config value
	Config::write($option, $value);

}

// Clean-up
unset($ini, $iniValues, $option, $value);

