<?php
/**
 * guardian-milter - Help
 *
 * Display help for command-line parameters and arguments
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage help
 */

// Display logo
echo chr(27) . "[34;1mguardian-milter" . chr(27) . "[0m v" . GUA_VER . " Copyright Noumenia 2023, GNU GPL v3.0\n";

// Premature exit for version only
if(
	isset($cmdParameters['V']) ||
	isset($cmdParameters['version'])
)
	exit();

echo "\nUsage: guardian-milter [OPTION]...\n\n";

// Array of help parameters
$help = array(
	array('short' => "V",	'long' => "version",			'desc' => "display version information only"),
	array('short' => "h",	'long' => "help",			'desc' => "display help about parameters"),
	array('short' => "v",	'long' => "verbose",			'desc' => "enable verbose output to stdout"),
	"",
	array('short' => "u",	'long' => "user=[USER]",		'desc' => "Effective user"),
	array('short' => "g",	'long' => "group=[GROUP]",		'desc' => "Effective group"),
	array('short' => "p",	'long' => "pid=[FILE]",			'desc' => "PID file"),
	array('short' => "l",	'long' => "processlimit=[NUM]",		'desc' => "Process limit"),
	array('short' => "c",	'long' => "connection=[STRING]",	'desc' => "Connection string"),
	array('short' => "a",	'long' => "autoload=[FILE]",		'desc' => "LibMilterPHP autoloader (controller/common.inc.php)"),
	array('short' => "i",	'long' => "ini=[FILE]",			'desc' => "Configuration file (/etc/gu-milter.ini)"),
	"",
	array('short' => "",	'long' => "addHeader",			'desc' => "Add Authentication-Results header with pass/fail information"),
	array('short' => "",	'long' => "reject",			'desc' => "Reject failed emails"),
	array('short' => "",	'long' => "removeName",			'desc' => "Remove From: header name part"),
	"",
	" The connection string can be a UNIX socket or an IPv4/IPv6 address/port.",
	" UNIX Socket...: unix:/run/gu-milter/guardian-milter.sock",
	" Address/port..: inet:8895@127.0.0.1"
);

// Display help
foreach($help as $h) {

	if(is_string($h))
		echo $h . "\n";
	elseif(!empty($h['short']))
		echo "  -" . $h['short'] . ",  --" . str_pad($h['long'], 20, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";
	else
		echo "       --" . str_pad($h['long'], 20, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";

}

exit();

