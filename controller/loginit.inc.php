<?php
/**
 * guardian-milter - Initialize log reporting
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage log
 */

// Set destination of log messages
if(
	isset($cmdParameters['v']) ||
	isset($cmdParameters['verbose'])
) {

	// Verbose output to syslog
	\Log::setDestination(new \LogDestinationSyslog(array('facility' => LOG_MAIL)), LOG_DEBUG);

	Config::write("verbose", true);

} else {

	// Display warnings or higher priority messages to syslog
	\Log::setDestination(new \LogDestinationSyslog(array('facility' => LOG_MAIL)), LOG_WARNING);

}

