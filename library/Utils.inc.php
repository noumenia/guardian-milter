<?php
/**
 * Utils class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage utils
 */
final class Utils {

	/**
	 * Transliterate popular characters from Cyrillic/Greek to Latin
	 * that obfuscate words like BANK, MAERSK and others.
	 * @param string $string
	 * @return string
	 */
	private static function transliterate(string $string): string
	{

		// Remove various characters that are used to trick text matching
		// \PCc - Match control characters
		// \PCn - Match unassigned characters
		// \PCs - Match UTF-8-invalid characters
		$string = preg_replace('/[^\PCc^\PCn^\PCs]/u', '', $string);

		// Return nothing on error
		if($string === null)
			return "";

		// Decode Cyrillic/Greek characters that obfuscate the words
		return str_replace(array(
			"\u{0412}",	// В
			"\u{0432}",	// в
			"\u{0392}",	// Β
			"\u{03b2}",	// β

			"\u{039d}",	// Ν
			"\u{03bd}",	// ν

			"\u{039a}",	// К
			"\u{041a}",	// К
			"\u{043a}",	// к
			"\u{03ba}",	// κ

			"\u{0406}",	// l
			"\u{217c}",	// ⅼ

			"\u{041c}",	// М
			"\u{043c}",	// м

			"\u{0410}",	// А
			"\u{043c}",	// а
			"\u{0430}",	// а

			"\u{0415}",	// Е
			"\u{0435}",	// е
			"\u{00cb}",	// Ë
			"\u{00eb}",	// ë
			"\u{0404}",	// Є
			"\u{0454}",	// є
			"\u{0510}",	// Ԑ
			"\u{0511}",	// ԑ

			"\u{0420}",	// Р
			"\u{0440}",	// р
			"\u{042f}",	// Я
			"\u{044f}",	// я

			"\u{0405}",	// Ѕ
			"\u{0455}",	// ѕ
			"\u{a682}",	// Ꚃ
			"\u{a683}",	// ꚃ

			"\u{041a}",	// К
			"\u{043a}",	// к

			"\u{0388}",	// Έ
			"\u{03ad}",	// έ
			"\u{0395}",	// Ε
			"\u{03b5}",	// ε
			"\u{03a4}",	// Τ
			"\u{03c4}",	// τ
			"\u{03a1}",	// Ρ
			"\u{03c1}",	// ρ
			"\u{0391}",	// Α
			"\u{0386}",	// Ά
			"\u{03b1}",	// α
			"\u{03ac}",	// ά
			"\u{03a0}",	// Π
			"\u{03c0}",	// π
			"\u{0396}",	// Ζ
			"\u{03b6}",	// ζ
			"\u{0394}",	// Δ
			"\u{03b4}",	// δ
			"\u{0397}",	// Η
			"\u{03b7}",	// η
			"\u{0389}",	// Ή
			"\u{03ae}",	// ή
			"\u{03a3}",	// Σ
			"\u{03c2}",	// ς
			"\u{03c3}",	// σ

			"\u{0399}",	// Ι
			"\u{038a}",	// Ί
			"\u{03b9}",	// ι
			"\u{03af}",	// ί

			"\u{039f}",	// Ο
			"\u{038c}",	// Ό
			"\u{03bf}",	// ο
			"\u{03cc}",	// ό

			"\u{039b}",	// Λ
			"\u{03bb}",	// λ

			"\u{039c}",	// Μ
			"\u{03bc}",	// μ

			"\u{03a5}",	// Υ
			"\u{038e}",	// Ύ
			"\u{0443}",	// y
			"\u{03c5}",	// υ
			"\u{03cd}",	// ύ

			"\u{0398}",	// Θ
			"\u{03b8}",	// θ

			"\u{03a9}",	// Ω
			"\u{03c9}",	// ω

			"\u{13a0}",	// Cherokee letter
			"\u{13de}",	// Cherokee letter
			"\u{13e2}",	// Cherokee letter
			"\u{13a1}",	// Cherokee letter

			"\u{03a7}",	// Coptic letter koppa / Greek letter Χ
			"\u{03c7}",	// Greek letter χ
			"\u{054f}"	// Armenian capital letter tiwn
		), array(
			"B",
			"B",
			"B",
			"b",

			"N",
			"n",

			"K",
			"K",
			"k",
			"k",

			"l",
			"l",

			"M",
			"m",

			"A",
			"a",
			"a",

			"E",
			"e",
			"E",
			"e",
			"E",
			"e",
			"E",
			"e",

			"R",
			"r",
			"R",
			"r",

			"S",
			"s",
			"S",
			"s",

			"K",
			"k",

			"E",
			"e",
			"E",
			"e",
			"T",
			"t",
			"R",
			"r",
			"A",
			"A",
			"a",
			"a",
			"P",
			"p",
			"Z",
			"z",
			"D",
			"d",
			"H",
			"h",
			"H",
			"h",
			"S",
			"s",
			"s",

			"I",
			"I",
			"i",
			"i",

			"O",
			"O",
			"o",
			"o",

			"L",
			"l",

			"M",
			"m",

			"I",
			"I",
			"y",
			"i",
			"i",

			"TH",
			"th",

			"O",
			"o",

			"D",
			"L",
			"P",
			"R",

			"X",
			"x",
			"S"
		), $string);

	}

	/**
	 * Decode base64 strings
	 * @param string $original
	 * @return string
	 */
	public static function decodeBase64(string $original): string
	{

		// Decode if possible else use the original string
		if(stripos($original, "=?") !== false) {

			$decoded = iconv_mime_decode($original, 2);
			if($decoded === false)
				$decoded = $original;

		} else {

			$decoded = $original;

		}

		// Alternative method when iconv_mime_decode() fails
		if(preg_match('/=\?utf-8\?\w\?/i', $decoded) === 1) {

			$decoded = preg_replace('/=\?utf-8\?\w\?/i', "", $decoded);
			if($decoded === null) {

				$decoded = $original;

			} else {

				$decoded = base64_decode($decoded);
				if($decoded === false)
					$decoded = $original;

			}

		}

		// Transliterate characters
		$decoded = self::transliterate($decoded);

		// Replace broken Apple Pay strings
		return str_replace(array("Arrle Ray", "Arrle"), array("Apple Pay", "Apple"), $decoded);

	}

	/**
	 * Extract the domain part from an email address
	 * @param string $email
	 * @return array{result: bool, reason: string}
	 */
	public static function extractEmailDomain(string $email): array
	{

		// Extract recipient domain (without brackets)
		if(strpos($email, "<") === false)
			$regexp = '/\@(.*?)/U';
		else
			$regexp = '/\@(.*?)>/U';

		$rc = preg_match($regexp, $email, $matches);
		if(
			$rc !== 1 ||
			!isset($matches[1])
		)
			return array(
				'result' => false,
				'reason' => "Could not extract the recipient domain"
			);

		return array(
			'result' => true,
			'reason' => $matches[1]
		);

	}

	/**
	 * Separate the name, email and domain parts
	 * @param string $email
	 * @return array{name: string, email: string, domain: string}
	 */
	public static function explodeEmail(string $email): array
	{

		// Mailparse module
		if(extension_loaded("mailparse")) {

			// Explode email address
			$mailparse = mailparse_rfc822_parse_addresses($email);

			if(
				isset($mailparse[0]['address'], $mailparse[0]['display']) &&
				is_string($mailparse[0]['address']) &&
				is_string($mailparse[0]['display'])
			) {

				$rc = self::extractEmailDomain($mailparse[0]['address']);
				if($rc['result'] === true)
					$domain = $rc['reason'];
				else
					$domain = "";

				return array(
					'name'		=> self::decodeBase64($mailparse[0]['display']),
					'email'		=> $mailparse[0]['address'],
					'domain'	=> $domain
				);

			}

		}

		// Explode email address
		$rc = preg_match('/(.*[^\s])\s*<\s*(.*[^\s])\s*>/', $email, $matches);
		if(
			$rc !== 1 ||
			!isset($matches[1], $matches[2])
		) {

			$rc = self::extractEmailDomain($email);
			if($rc['result'] === true)
				$domain = $rc['reason'];
			else
				$domain = "";

			return array(
				'name'		=> "",
				'email'		=> $email,
				'domain'	=> $domain
			);

		}

		$rc = self::extractEmailDomain($matches[2]);
		if($rc['result'] === true)
			$domain = $rc['reason'];
		else
			$domain = "";

		return array(
			'name'		=> self::decodeBase64($matches[1]),
			'email'		=> $matches[2],
			'domain'	=> $domain
		);

	}

	/**
	 * Unfold header lines
	 * @param string $lines
	 * @return string
	 */
	public static function unfold(string $lines): string
	{

		// Unfold header lines
		$rc = preg_replace('/(\r\n|\r|\n)\s+/ims', " ", $lines);
		if(!is_string($rc))
			return $lines;
		else
			return $rc;

	}

	/**
	 * Find MIME header sections
	 * @param string $mimeBoundary
	 * @param string &$message
	 * @return array<string>
	 */
	public static function mimeHeaderSections(string $mimeBoundary, string &$message): array
	{

		// Use the boundary string to find MIME header sections
		$rc = preg_match_all(
			'/^--' . preg_quote($mimeBoundary, "/") . '(\r\n|\r|\n)(.*?)(\r\n\r\n|\r\r|\n\n)/ims',
			$message,
			$matches
		);

		if(
			$rc !== false &&
			$rc >= 1 &&
			sizeof($matches[2]) > 0
		)
			return array_map(array("Utils", "unfold"), $matches[2]);
		else
			return array();

	}

	/**
	 * Find MIME file attachments
	 * @param string $mimeBoundary
	 * @param string &$message
	 * @return array<string|false>
	 */
	public static function mimeFileAttachments(string $mimeBoundary, string &$message): array
	{

		// Found files
		$files = array();

		// Use the boundary string to find MIME header sections
		$sections = self::mimeHeaderSections($mimeBoundary, $message);

		// Loop matches
		foreach($sections as &$section) {

			// Extract filename
			$rc = preg_match('/^Content-(Disposition|Type).*name\s*=\s*"?([^;]*(\.|=2E)(.*?))(\?=)?"?\s*(;|$)/ims', $section, $file);
			if(
				$rc !== false &&
				isset($file[2])
			)
				// Decode MIME and add to the found files array
				$files[] = iconv_mime_decode($file[2], 2, "UTF-8");

		}

		return $files;

	}

	/**
	 * Execute an HTTP/HTTPS GET request
	 * @param string $url
	 * @param array<string> $headers
	 * @param array<string, string> $parameters
	 * @return string
	 */
	public static function httpGet(string $url, array $headers = array(), array $parameters = array()): string
	{

		// Input validation
		if(empty($url))
			return "";

		// Initialize curl
		$curl = curl_init();
		if($curl === false)
			return "";

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);

		// Request headers
		if(sizeof($headers) > 0)
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		// URL parameters
		if(sizeof($parameters) > 0)
			$url .= "?" . http_build_query($parameters);

		// Set the URL
		curl_setopt($curl, CURLOPT_URL, $url);

		// Download
		$data = curl_exec($curl);

		// Get the HTTP code
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		// Close curl
		curl_close($curl);

		// Error detection
		if(
			$httpcode !== 200 ||
			is_bool($data)
		)
			return "";

		return $data;

	}

	/**
	 * AbuseIPDB integration
	 * @see https://www.abuseipdb.com/
	 * @see https://docs.abuseipdb.com/#check-endpoint
	 * @param string $ip
	 * @param string $apiKey
	 * @return object|false
	 */
	public static function abuseIpDb(string $ip, string $apiKey): object|false
	{

		// AbuseIPDB check
		$json = Utils::httpGet(
			// API v2 URL
			"https://api.abuseipdb.com/api/v2/check",
			// Headers
			array(
				"Key: " . $apiKey,
				"Accept: application/json"
			),
			// Request parameters
			array(
				'ipAddress'	=> $ip,
				'maxAgeInDays'	=> '30'
			)
		);

		// Request failed
		if(empty($json))
			return false;

		// Decode JSON
		$json = json_decode($json);
		if(
			!is_object($json) ||
			!property_exists($json, "data")
		)
			return false;
		else
			return $json->data;

	}

}

