<?php
/**
 * Daemon class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage daemon
 */
final class Daemon extends \libMilterPHP\Milter {

	/**
	 * Email
	 * @var ?Email
	 */
	private ?Email $email = null;

	/**
	 * Reply with SMFIR_CONTINUE
	 * @return void
	 */
	private function replyContinue(): void
	{

		// Reply
		$reply = array(

			// Set the command (char)
			'command'	=> SMFIR_CONTINUE,

			// Set the data (chars of uint32 size)
			'data'		=> array()

		);

		$this->reply($reply);

	}

	/**
	 * Reply with SMFIR_REPLYCODE 550 5.7.1
	 * @return void
	 */
	private function replyReject(): void
	{

		// Reply
		$reply = array(

			// Set the command (char)
			'command'	=> SMFIR_REPLYCODE,

			// Set the data (chars of uint32 size)
			'data'		=> array(
				'smtpcode'	=> "550",
				'space'		=> " ",
				'text'		=> "5.7.1 Blocked by guardian-milter"
			)

		);

		$this->reply($reply);

	}

	/**
	 * Reply with SMFIR_ADDHEADER
	 * @param string $key
	 * @param string $value
	 * @return void
	 */
	private function replyAddHeader(string $key, string $value): void
	{

		// Add header
		$reply = array(

			// Set the command (char)
			'command'	=> SMFIR_ADDHEADER,

			// Set the data (chars of uint32 size)
			'data'		=> array('name' => $key, 'value' => $value)

		);

		$this->reply($reply);

	}

	/**
	 * Reply with SMFIR_ADDRCPT
	 * @param string $email
	 * @return void
	 */
	private function replyAddRecipient(string $email): void
	{

		// Add header
		$reply = array(

			// Set the command (char)
			'command'	=> SMFIR_ADDRCPT,

			// Set the data (chars of uint32 size)
			'data'		=> array('rcpt' => $email)

		);

		$this->reply($reply);

	}

	/**
	 * Reply with SMFIR_CHGHEADER
	 * @param string $key
	 * @param string $value
	 * @return void
	 */
	private function replyChangeHeader(string $key, string $value): void
	{

		// Add header
		$reply = array(

			// Set the command (char)
			'command'	=> SMFIR_CHGHEADER,

			// Set the data (chars of uint32 size)
			'data'		=> array('index' => 1, 'name' => $key, 'value' => $value)

		);

		$this->reply($reply);

	}

	/**
	 * Save the Email object as a JSON text file
	 * @return void
	 */
	private function saveToJson(): void
	{

		// Input validation
		if($this->email === null)
			return;

		// Store failed emails in JSON format
		$storeFailPath = Config::readStr("storeFailPath");
		if(
			empty($storeFailPath) ||
			!is_dir($storeFailPath)
		)
			return;

		// Generate a filename based on the current time and the Message-ID header (or From domain in case there is no Message-ID)
		if(
			isset($this->email->headers['MESSAGE-ID']) &&
			!empty($this->email->headers['MESSAGE-ID'])
		)
			$mid = preg_replace('/[^a-zA-Z0-9]+/u', "", $this->email->headers['MESSAGE-ID']);
		else
			$mid = $this->email->envelopeFromDomain;

		$jsonFile = $storeFailPath . "/" . time() . "-" . $mid . ".json";

		// Avoid overwriting files
		if(!is_file($jsonFile)) {

			$jsonData = json_encode($this->email, JSON_INVALID_UTF8_IGNORE | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
			if($jsonData === false) {

				Log::error($this->email->queueId . ": ERROR - Failed to encode JSON data");
				return;

			}

			// Email to JSON format
			$rc = @file_put_contents($jsonFile, $jsonData);
			if($rc === false)
				Log::error($this->email->queueId . ": ERROR - Failed to write JSON data to " . $storeFailPath);

		}

		// Clean-up
		unset($storeFailPath, $mid, $jsonFile);

	}

	/**
	 * SMFIC_MACRO - Define macros
	 * @param array{size: int, command: string, binary: string, macroCommand: string, macros: array<string>} $message Message array
	 * @return void
	 */
	public function smficMacro(array $message): void
	{

		// Start of new email
		if($this->email === null)
			$this->email = new Email();

		// Set macro
		$this->email->setMacro($message['macroCommand'], $message['macros']);

	}

	/**
	 * SMFIC_CONNECT - SMTP connection information - use SMFIP_NOCONNECT to suppress
	 * @param array{size: int, command: string, binary: string, hostname: string, protocolFamily: string, port?: int, address?: string} $message Message array
	 * @return void
	 */
	public function smficConnect(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		// Set SMTP connection information
		$rc = $this->email->setConnection($message);
		if(
			$rc === false &&
			Config::read("reject") === true
		) {

			// Store failed emails in JSON format
			$this->saveToJson();

			$this->replyReject();

		}

		$this->replyContinue();

	}

	/**
	 * SMFIC_HELO - HELO/EHLO name - use SMFIP_NOHELO to suppress
	 * @param array{size: int, command: string, binary: string, helo: string} $message Message array
	 * @return void
	 */
	public function smficHelo(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		// Set SMTP connection information
		$rc = $this->email->setHelo($message['helo']);
		if(
			$rc === false &&
			Config::read("reject") === true
		) {

			// Store failed emails in JSON format
			$this->saveToJson();

			$this->replyReject();

		}

		$this->replyContinue();

	}

	/**
	 * SMFIC_MAIL - MAIL FROM: information - use SMFIP_NOMAIL to suppress
	 * @param array{size: int, command: string, binary: string, args: array<string>} $message Message array
	 * @return void
	 */
	public function smficMail(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		$rc = $this->email->setEnvelopeFrom($message);
		if(
			$rc === false &&
			Config::read("reject") === true
		) {

			// Store failed emails in JSON format
			$this->saveToJson();

			$this->replyReject();

		}

		$this->replyContinue();

	}

	/**
	 * SMFIC_RCPT - RCPT TO: information - use SMFIP_NORCPT to suppress
	 * @param array{size: int, command: string, binary: string, args: array<string>} $message Message array
	 * @return void
	 */
	public function smficRcpt(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		$rc = $this->email->setEnvelopeRcpt($message);
		if(
			$rc === false &&
			Config::read("reject") === true
		) {

			// Store failed emails in JSON format
			$this->saveToJson();

			$this->replyReject();

		}

		$this->replyContinue();

	}

	/**
	 * SMFIC_HEADER - Mail header - use SMFIP_NOHDRS to suppress
	 * @param array{size: int, command: string, binary: string, name: string, value: string} $message Message array
	 * @return void
	 */
	public function smficHeader(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		$rc = $this->email->setHeader($message['name'], $message['value']);
		if(
			$rc === false &&
			Config::read("reject") === true
		) {

			// Store failed emails in JSON format
			$this->saveToJson();

			$this->replyReject();

		}

		$this->replyContinue();

	}

	/**
	 * SMFIC_EOH - End of headers marker - use SMFIP_NOEOH to suppress
	 * @param array{size: int, command: string, binary: string} $message Message array
	 * @return void
	 */
	public function smficEoh(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		$this->replyContinue();

	}

	/**
	 * SMFIC_BODY - Body chunk - use SMFIP_NOBODY to suppress
	 *
	 * The current approach may fail to detect the MIME part
	 * when the default milter buffer (65535 bytes) splits
	 * the MIME boundary in multiple parts. This can be avoided
	 * by using the following code to store the entire message
	 * body into memory. We currently avoid doing that for
	 * performance reasons.
	 *
	 * 		$rc = $this->email->appendBody($message['buf']);
	 * 		if(
	 * 			$rc === false &&
	 * 			Config::read("reject") === true
	 * 		) {
	 *
	 * 			// Store failed emails in JSON format
	 * 			$this->saveToJson();
	 *
	 * 			$this->replyReject();
	 *
	 * 		}
	 *
	 * 		$this->replyContinue();
	 *
	 * @param array{size: int, command: string, binary: string, buf: string} $message Message array
	 * @return void
	 */
	public function smficBody(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		// Run body scans
		$rc = $this->email->scanBody($message['buf']);
		if(
			$rc === false &&
			Config::read("reject") === true
		) {

			// Store failed emails in JSON format
			$this->saveToJson();

			$this->replyReject();

		}

		$this->replyContinue();

	}

	/**
	 * SMFIC_BODYEOB - End of body marker
	 * @param array{size: int, command: string, binary: string} $message Message array
	 * @return void
	 */
	public function smficBodyeob(array $message): void
	{

		// Input validation
		if($this->email === null)
			return;

		// Run checks
		$this->email->runChecks();

		// Remove From: header name part
		if(Config::read("removeName") === true)
			$this->replyChangeHeader("From", "<" . $this->email->headerFrom . ">");

		// Handle result pass
		if($this->email->result === "pass") {

			if(Config::read("addHeader") === true) {

				$hostname = gethostname();
				if($hostname === false)
					$hostname = "unknown";

				// Add header
				$this->replyAddHeader("Authentication-Results", $hostname . "; guardian=pass");

			}

			Log::warning($this->email->queueId . ": PASS");

			$this->replyContinue();

			return;

		}

		// Store failed emails in JSON format
		$this->saveToJson();

		// Reject immediately else continue with a fail
		if(Config::read("reject") === true) {

			// Handle result reject
			foreach($this->email->reason as &$reason)
				Log::warning($this->email->queueId . ": REJECT - " . $reason);

			$this->replyReject();

			return;

		} else {

			// Handle result fail
			foreach($this->email->reason as &$reason)
				Log::warning($this->email->queueId . ": FAIL - " . $reason);

		}

		// Add header
		if(Config::read("addHeader") === true) {

			$hostname = gethostname();
			if($hostname === false)
				$hostname = "unknown";

			foreach($this->email->reason as &$reason)
				$this->replyAddHeader("Authentication-Results", $hostname . "; guardian=fail (" . $reason . ")");

		}

		// Add a recipient
		$copyFailTo = Config::readStr("copyFailTo");
		if(!empty($copyFailTo))
			$this->replyAddRecipient($copyFailTo);

		$this->replyContinue();

	}

	/**
	 * SMFIC_ABORT - Abort current filter checks
	 * @param array{size: int, command: string, binary: string} $message Message array
	 * @return void
	 */
	public function smficAbort(array $message): void
	{

		// Keep the connection information as per the milter protocol v2
		if($this->email !== null)
			$connection = $this->email->connection;
		else
			$connection = null;

		// Start of new email
		$this->email = new Email();

		// Restore the connection information
		if($connection !== null)
			$this->email->connection = $connection;

		// Clean-up
		unset($connection);

	}

	/**
	 * SMFIC_QUIT - Quit milter communication
	 * @param array{size: int, command: string, binary: string} $message Message array
	 * @return void
	 */
	public function smficQuit(array $message): void
	{

		// Clean-up
		$this->email = null;

	}

}

