<?php
/**
 * Config class
 *
 * Keeps a global configuration in key/value pair format
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage config
 */
final class Config {

	/**
	 * Global configuration array
	 * @var array<string, string|int|bool|array<string>>
	 */
	private static array $configuration = array();

	/**
	 * Global log array
	 * @var array<int, string>
	 */
	public static array $log = array();

	/**
	 * Set global configuration array with default values
	 * @return void
	 */
	public static function setDefaults(): void
	{

		Log::debug("guardian-milter: Set configuration default values");

		// Set global configuration array with default values
		self::$configuration = array(
			'version'				=> GUA_VER,
			'verbose'				=> false,
			'ini'					=> dirname(__DIR__) . "/gu-milter.ini",

			// Settings settable in the INI file
			'effectiveUser'				=> "gu-milter",
			'effectiveGroup'			=> "gu-milter",
			'filePid'				=> "/run/gu-milter/gu-milter.pid",
			'processLimit'				=> 1024,
			'connection'				=> "inet:8895@127.0.0.1",
			'addHeader'				=> true,
			'copyFailTo'				=> "",
			'reject'				=> false,
			'removeName'				=> false,
			'storeFailPath'				=> "",
			'checks'				=> array(),
			'connect'				=> array(),
			'helo'					=> array(),
			'envelopeRcpt'				=> array(),
			'extBlacklist'				=> "",
			'abuseIpDbKey'				=> "",
			'abuseIpDbBlockOnConfidenceScore'	=> 50,
			'abuseIpDbBlacklistCountries'		=> array()
		);

	}

	/**
	 * Isset for the global configuration
	 * @param string $key Configuration key
	 * @return bool
	 */
	public static function isset(string $key): bool
	{

		return isset(self::$configuration[$key]);

	}

	/**
	 * Read a key/value pair from the global configuration
	 * @param string $key Configuration key
	 * @param bool $ignore Ignore errors if the key does not exist
	 * @return string|int|bool|array<string>
	 */
	public static function read(string $key, bool $ignore = false): string|int|bool|array
	{

		// Return global configuration value
		if(isset(self::$configuration[$key]))
			return self::$configuration[$key];

		if($ignore === false)
			Log::error("guardian-milter: Configuration key not found: " . $key);

		return false;

	}

	/**
	 * Read a string key/value pair from the global configuration
	 * @param string $key Configuration key
	 * @return string
	 */
	public static function readStr(string $key): string
	{

		// Return global configuration value
		if(
			isset(self::$configuration[$key]) &&
			is_string(self::$configuration[$key])
		)
			return self::$configuration[$key];
		else
			return "";

	}

	/**
	 * Insert or update a key/value pair in the global configuration
	 * @param string $key Configuration key
	 * @param string|int|bool|array<string> $value Configuration value
	 * @return bool
	 */
	public static function write(string $key, string|int|bool|array $value): bool
	{

		// Set variable
		self::$configuration[$key] = $value;

		return true;

	}

}

