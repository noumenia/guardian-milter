<?php
/**
 * Email class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage email
 */
final class Email {

	/**
	 * Queue ID
	 * @var string
	 */
	public string $queueId = "guardian-milter";

	/**
	 * Array of macros, key is the macro command character
	 * @var array<string, array<string, string>>
	 */
	public array $macros = array();

	/**
	 * Connection information
	 * @var array{hostname: string, protocolFamily: string, port?: int, address: string}
	 */
	public array $connection = array(
		'hostname'		=> "",
		'protocolFamily'	=> "",
		'port'			=> 0,
		'address'		=> ""
	);

	/**
	 * HELO/EHLO
	 * @var string
	 */
	public string $helo = "";

	/**
	 * Envelope From
	 * @var string
	 */
	public string $envelopeFrom = "";

	/**
	 * Envelope From domain (extracted from envelopeFrom)
	 * @var string
	 */
	public string $envelopeFromDomain = "";

	/**
	 * Envelope To (recipient)
	 * @var string
	 */
	public string $envelopeRcpt = "";

	/**
	 * Recipient domain (extracted from envelopeRcpt)
	 * @var string
	 */
	public string $envelopeRcptDomain = "";

	/**
	 * Headers
	 * @var array<string, string>
	 */
	public array $headers = array();

	/**
	 * Header From:
	 * @var string
	 */
	public string $headerFrom = "";

	/**
	 * Header From: name part
	 * @var string
	 */
	public string $headerFromName = "";

	/**
	 * Header From: name part, base64 decoded
	 * @var string
	 */
	public string $headerFromNameDecoded = "";

	/**
	 * Header From: domain part
	 * @var string
	 */
	public string $headerFromDomain = "";

	/**
	 * Header Subject:
	 * @var string
	 */
	public string $headerSubject = "";

	/**
	 * Header Subject:, base64 decoded
	 * @var string
	 */
	public string $headerSubjectDecoded = "";

	/**
	 * MIME boundary extracted from the Content-Type: header
	 * @var string
	 */
	public string $mimeBoundary = "";

	/**
	 * Body, populated only when SMFIP_NOBODY is
	 * not given within the $ignoredContent array
	 * @var string
	 */
	public string $body = "";

	/**
	 * Result string after running checks (pass/fail)
	 * @var string
	 */
	public string $result = "pass";

	/**
	 * Reason string(s) returned by the failed check
	 * @var array<string>
	 */
	public array $reason = array();

	/**
	 * Set macro
	 * @param string $macroCommand Macro command character
	 * @param array<string> $macroData Macro array of data
	 * @return void
	 */
	public function setMacro(string $macroCommand, array $macroData): void
	{

		// Input validation
		if(strlen($macroCommand) !== 1)
			return;

		// Create an array for this macro command
		if(!isset($this->macros[$macroCommand]))
			$this->macros[$macroCommand] = array();

		for($i = 0; $i < sizeof($macroData); $i++) {

			// Input validation
			if(
				(
					isset($macroData[$i]) &&
					empty($macroData[$i])
				) ||
				!isset($macroData[($i + 1)])
			)
				continue;

			// Remove brackets
			$macroData[$i] = str_replace(array("{", "}"), "", $macroData[$i]);

			// Avoid overwriting existing entries, but may overwrite duplicates
			if(isset($this->macros[$macroCommand][$macroData[$i]]))
				$macroData[$i] .= "_DUPLICATE";

			$this->macros[$macroCommand][$macroData[$i]] = $macroData[($i + 1)];

			// Skip one
			$i++;

		}

		// Keep the queue ID separate for logging purposes
		if(
			$this->queueId === "guardian-milter" &&
			isset($this->macros[$macroCommand]['i']) &&
			!empty($this->macros[$macroCommand]['i'])
		)
			$this->queueId = $this->macros[$macroCommand]['i'];

		return;

	}

	/**
	 * Set SMTP connection information
	 * @param array{size: int, command: string, binary: string, hostname: string, protocolFamily: string, port?: int, address?: string} $message
	 * @return bool
	 */
	public function setConnection(array $message): bool
	{

		// Input validation
		if(!isset($message['hostname'], $message['protocolFamily'])) {

			$this->result = "fail";
			$this->reason[] = "Input validation";

			return false;

		}

		$this->connection['hostname'] = $message['hostname'];
		$this->connection['protocolFamily'] = $message['protocolFamily'];
		if(isset($message['port']))
			$this->connection['port'] = $message['port'];
		if(isset($message['address']))
			$this->connection['address'] = $message['address'];

		return true;

	}

	/**
	 * Set HELO/EHLO
	 * @param string $helo
	 * @return bool
	 */
	public function setHelo(string $helo): bool
	{

		$this->helo = $helo;

		return true;

	}

	/**
	 * Set MAIL (envelope) FROM: information
	 * @param array{size: int, command: string, binary: string, args: array<string>} $message Message array
	 * @return bool
	 */
	public function setEnvelopeFrom(array $message): bool
	{

		// Input validation
		if(
			!isset($message['args'][0]) ||
			empty($message['args'][0])
		) {

			$this->result = "fail";
			$this->reason[] = "Input validation";

			return false;

		}

		// Duplicate MAIL FROM: - REJECT MAIL
		if($this->envelopeFrom !== "") {

			Log::warning($this->queueId . ": REJECT - Duplicate MAIL FROM");

			$this->result = "fail";
			$this->reason[] = "Duplicate MAIL FROM";

			return false;

		}

		// Set the envelope MAIL FROM:
		$this->envelopeFrom = str_replace(array("<", ">"), "", $message['args'][0]);

		// Extract domain part
		$rc = Utils::extractEmailDomain($message['args'][0]);
		if($rc['result'] === true) {

			$this->envelopeFromDomain = $rc['reason'];

		} else {

			Log::warning($this->queueId . ": REJECT - Failed to extract the domain part from the MAIL FROM envelope");

			$this->result = "fail";
			$this->reason[] = "Failed to extract the domain part from the MAIL FROM envelope";

			return false;

		}

		return true;

	}

	/**
	 * Set RCPT (envelope) TO: information
	 * @param array{size: int, command: string, binary: string, args: array<string>} $message Message array
	 * @return bool
	 */
	public function setEnvelopeRcpt(array $message): bool
	{

		// Input validation
		if(
			!isset($message['args'][0]) ||
			empty($message['args'][0])
		) {

			$this->result = "fail";
			$this->reason[] = "Input validation";

			return false;

		}

		// Set the envelope RCPT TO:
		$this->envelopeRcpt = str_replace(array("<", ">"), "", $message['args'][0]);

		// Skip the next part if there is no @ character to denote a domain
		if(strpos($message['args'][0], "@") === false)
			return true;

		// Extract domain part
		$rc = Utils::extractEmailDomain($message['args'][0]);
		if($rc['result'] === true) {

			$this->envelopeRcptDomain = $rc['reason'];

		} else {

			Log::warning($this->queueId . ": REJECT - Failed to extract the domain part from the RCPT TO envelope");

			$this->result = "fail";
			$this->reason[] = "Failed to extract the domain part from the RCPT TO envelope";

			return false;

		}

		return true;

	}

	/**
	 * Set header key/value pair
	 * @param string $key Header key
	 * @param string $value Header value
	 * @return bool
	 */
	public function setHeader(string $key, string $value): bool
	{

		// Input validation
		if(empty($key)) {

			$this->result = "fail";
			$this->reason[] = "Input validation";

			return false;

		}

		// Force key into uppercase
		$key = strtoupper($key);

		// Start with a zero counter
		$counter = 0;
		$tmpKey = $key;

		// Loop over existing header keys
		while(isset($this->headers[$tmpKey])) {

			$counter++;

			$tmpKey = $key . $counter;

		}

		// Override key
		if(isset($this->headers[$key]))
			$key = $tmpKey;

		// Set key/value pair
		$this->headers[$key] = $value;

		// Check for the "From:" header
		if($key === "FROM") {

			// Keep the "From:" header
			$rc = Utils::explodeEmail($value);
			$this->headerFromName = $rc['name'];
			$this->headerFromNameDecoded = $rc['name'];
			$this->headerFrom = $rc['email'];
			$this->headerFromDomain = $rc['domain'];

		}

		// Check for the "Subject:" header
		elseif($key === "SUBJECT") {

			// Keep the "Subject:" header
			$this->headerSubject = $value;

			// Decode if possible
			$this->headerSubjectDecoded = Utils::decodeBase64($value);

		}

		// Check for the "Content-Type:" header
		elseif($key === "CONTENT-TYPE") {

			// Parse the MIME boundary
			$rc = preg_match('/boundary="(.*?)"/ims', $value, $matches);
			if(
				$rc === 1 &&
				isset($matches[1]) &&
				!empty($matches[1])
			)
				$this->mimeBoundary = $matches[1];

		}

		return true;

	}

	/**
	 * Scan body part (chunks of 65535 bytes as per milter default)
	 * @param string $bodyPart
	 * @return bool
	 */
	public function scanBody(string $bodyPart): bool
	{

		// Load the file extension blacklist
		$extBlacklist = Config::readStr("extBlacklist");

		// Return early if there is no MIME boundary or the blacklist is empty
		if(
			empty($this->mimeBoundary) ||
			empty($extBlacklist)
		)
			return true;

		// Find MIME file attachments
		$files = Utils::mimeFileAttachments($this->mimeBoundary, $bodyPart);

		// Loop files
		foreach($files as &$file) {

			// Handle broken MIME encoding
			if($file === false) {

				$this->result = "fail";
				$this->reason[] = "Broken MIME encoding";

				return false;

			} else {

				$rc = preg_match('/\.(' . $extBlacklist . ')$/i', $file);
				if($rc === 1) {

					$this->result = "fail";
					$this->reason[] = "Blacklisted file extension";

					return false;

				}

			}

		}

		return true;

	}

	/**
	 * Append body part to the whole body variable
	 * @param string $bodyPart
	 * @return bool
	 */
	public function appendBody(string $bodyPart): bool
	{

		$this->body .= $bodyPart;

		return true;

	}

	/**
	 * Run all checks
	 * @return bool
	 */
	public function runChecks(): bool
	{

		// Get configured checks
		$checks = Config::read("checks");
		if(!is_array($checks)) {

			// Set result/reason on this
			$this->result = "fail";
			$this->reason[] = "Invalid Checks array";

			return false;

		}

		// Get all checks supported by the Checks class and also enabled by the configuration
		$checks = array_intersect($checks, get_class_methods("Checks"));

		// Loop checks
		foreach($checks as $check) {

			// Execute check
			/** @var array{result: string, reason: string} $rc */
			$rc = Checks::$check($this);
			if($rc['result'] === "fail") {

				// Set result/reason on this
				$this->result = "fail";
				$this->reason[] = $rc['reason'];

			}

		}

		if($this->result === "pass")
			return true;
		else
			return false;

	}

}

