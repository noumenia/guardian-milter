<?php
/**
 * Checks class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package guardian-milter
 * @subpackage checks
 */
final class Checks {

	/**
	 * Run regular expressions against the connection hostname/IP strings
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function connectRegexp(Email &$email): array
	{

		// Input validation
		$regexp = Config::read("connect");
		if(!is_array($regexp))
			return array(
				'result' => "fail",
				'reason' => "Invalid connect array"
			);

		// Loop connect regular expressions
		foreach($regexp as $r) {

			// Hostname
			$rc = preg_match('/' . $r . '/i', $email->connection['hostname']);
			if($rc === 1)
				return array(
					'result' => "fail",
					'reason' => "Connecting hostname matched regular expression"
				);

			// IP address
			$rc = preg_match('/' . $r . '/i', $email->connection['address']);
			if($rc === 1)
				return array(
					'result' => "fail",
					'reason' => "Connecting IP address matched regular expression"
				);

		}

		return array(
			'result' => "pass",
			'reason' => ""
		);

	}

	/**
	 * Run regular expressions against the HELO/EHLO string
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function heloRegexp(Email &$email): array
	{

		// Input validation
		$regexp = Config::read("helo");
		if(!is_array($regexp))
			return array(
				'result' => "fail",
				'reason' => "Invalid helo array"
			);

		// Loop HELO/EHLO regular expressions
		foreach($regexp as $r) {

			$rc = preg_match('/' . $r . '/i', $email->helo);
			if($rc === 1)
				return array(
					'result' => "fail",
					'reason' => "HELO/EHLO matched regular expression"
				);

		}

		return array(
			'result' => "pass",
			'reason' => ""
		);

	}

	/**
	 * Run regular expressions against the envelope RCPT string
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function envelopeRcptRegexp(Email &$email): array
	{

		// Input validation
		$regexp = Config::read("envelopeRcpt");
		if(!is_array($regexp))
			return array(
				'result' => "fail",
				'reason' => "Invalid envelope RCPT array"
			);

		// Loop envelope RCPT regular expressions
		foreach($regexp as $r) {

			$rc = preg_match('/' . $r . '/i', $email->envelopeRcpt);
			if($rc === 1)
				return array(
					'result' => "fail",
					'reason' => "Envelope RCPT matched regular expression"
				);

		}

		return array(
			'result' => "pass",
			'reason' => ""
		);

	}

	/**
	 * Check if the recipient domain is within the name part of the From: header
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function domainInFromName(Email &$email): array
	{

		// Input validation
		if(empty($email->envelopeRcptDomain))
			return array(
				'result' => "pass",
				'reason' => ""
			);

		// Create a regular expression pattern to match the domain
		$pattern = "/\b" . str_replace(".", "\.", $email->envelopeRcptDomain) . "\b/i";

		$rc = preg_match($pattern, $email->headerFromNameDecoded);
		if($rc === 1)
			return array(
				'result' => "fail",
				'reason' => "Recipient domain found within the name part of the From: header"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check if the recipient domain is within the Subject: header
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function domainInSubject(Email &$email): array
	{

		// Input validation
		if(empty($email->envelopeRcptDomain))
			return array(
				'result' => "pass",
				'reason' => ""
			);


		// Look for first part domain
		$pos = mb_strpos($email->envelopeRcptDomain, ".");
		if($pos !== false) {

			// Extract first part
			$firstPart = str_replace(".", "\.", mb_substr($email->envelopeRcptDomain, 0, $pos));

			// Create regular expression patterns to match the domain
			$patterns = array(
				"/" . $firstPart . " Inv(i|o)?(o|i)?ce Approval Notification/i",
				"/" . $firstPart . " Server Maintenance/i",
				"/" . $firstPart . " Staff Payroll/i",
				"/NEXIN " . $firstPart . "/i",
				"/Keep Same Credential For " . $firstPart . "/i"
			);

			foreach($patterns as $key => $pattern) {

				if(
					stripos($email->headerSubjectDecoded, "domain") === false &&
					preg_match($pattern, $email->headerSubjectDecoded) === 1
				)
					return array(
						'result' => "fail",
						'reason' => "Recipient domain found within the Subject: header, matching fake notification [" . $key . "]"
					);

			}

		}

		// Create a regular expression pattern to match the domain
		$pattern = "/\b" . str_replace(".", "\.", $email->envelopeRcptDomain) . "\b/i";

		if(
			stripos($email->headerSubjectDecoded, "domain") === false &&
			preg_match($pattern, $email->headerSubjectDecoded) === 1
		)
			return array(
				'result' => "fail",
				'reason' => "Recipient domain found within the Subject: header"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check if the recipient name (user part of the email) is within the Subject: header
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function recipientInSubject(Email &$email): array
	{

		// Input validation
		if(empty($email->envelopeRcpt))
			return array(
				'result' => "pass",
				'reason' => ""
			);

		// Look for first part name
		$pos = mb_strpos($email->envelopeRcpt, "@");
		if($pos === false) {

			return array(
				'result' => "fail",
				'reason' => "Invalid envelope RCPT missing @ symbol"
			);

		}

		// Extract first part
		$firstPart = mb_substr($email->envelopeRcpt, 0, $pos);

		if(
			(
				stripos($email->headerSubjectDecoded, $firstPart . " Notification Access") !== false ||
				stripos($email->headerSubjectDecoded, "Update Your Account " . $firstPart) !== false ||
				stripos($email->headerSubjectDecoded, "Server Authentication Failure For " . $firstPart) !== false ||
				stripos($email->headerSubjectDecoded, "Password Expiration for " . $firstPart) !== false ||
				stripos($email->headerSubjectDecoded, "Password for " . $firstPart) !== false ||
				stripos($email->headerSubjectDecoded, "Action Required: " . $firstPart) !== false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Recipient name found within the Subject: header, matching fake notification"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake WeTransfer
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeWeTransfer(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "wetransfer") !== false &&
			stripos($email->headerFromDomain, "wetransfer.com") === false
		)
			return array(
				'result' => "fail",
				'reason' => "Fake WeTransfer"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake bank domain
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeBank(Email &$email): array
	{

		// Loop countries
		foreach(BANK_DOMAINS as $country => &$bankDomains) {

			// Loop banks
			/** @var string $bankDomain */
			foreach($bankDomains as &$bankDomain) {

				// Remove the ending part
				$bankDomainNoEnd = preg_replace('/\..*/i', "", $bankDomain);
				if($bankDomainNoEnd === null)
					$bankDomainNoEnd = $bankDomain;

				// Test for the full domain (fakebank.com within From name, but not in From domain)
				if(
					stripos($email->headerFromNameDecoded, $bankDomain) !== false &&
					stripos($email->envelopeFromDomain, $bankDomain) === false
				) {

					return array(
						'result' => "fail",
						'reason' => "Fake Bank #1 (" . $bankDomain . ")"
					);

				}

				// Get partial domain only (turn fakebank.com into fakebank)
				$bankDomainPartial = preg_replace('/\..*/i', "", $bankDomain);

				// Test for partial domain + group|system|web string combinations
				if(
					$bankDomainPartial !== null &&
					preg_match('/' . $bankDomainPartial . '.?(GROUP|SYSTEM|WEB)/i', $email->headerFromNameDecoded) === 1 &&
					stripos($email->envelopeFromDomain, $bankDomain) === false
				) {

					return array(
						'result' => "fail",
						'reason' => "Fake Bank #2 (" . $bankDomain . ")"
					);

				}

				// Test for BANK|apaiteitai|ipenthimish strings in From
				if(
					!in_array($email->headerFromDomain, $bankDomains) &&
					(
						stripos($email->headerFromNameDecoded, "BANK") !== false ||
						stripos($email->headerFromNameDecoded, "apaiteitai") !== false ||
						stripos($email->headerFromNameDecoded, "apaitoime") !== false ||
						stripos($email->headerFromNameDecoded, "ipenthimish") !== false ||
						stripos($email->headerFromNameDecoded, "enhmerosh") !== false ||
						stripos($email->headerFromNameDecoded, "prosbash") !== false
					)
				) {

					// Get partial name only
					$bankDomainPartial = str_ireplace(
						array("-", "BANK", "I"),
						array("", ".?(?:e-|WEB ?)?BANK", "(I|l)"),
						$bankDomainNoEnd
					);

					// Test for partial domain with space (fake e-bank|fake webbank)
					if(
						preg_match('/' . $bankDomainPartial . '/i', $email->headerFromNameDecoded) === 1 &&
						stripos($email->envelopeFromDomain, $bankDomain) === false
					) {

						return array(
							'result' => "fail",
							'reason' => "Fake Bank #3 (" . $bankDomain . ")"
						);

					}

					// Test for BANK string in Subject
					if(stripos($email->headerSubjectDecoded, "BANK") !== false) {

						// Get partial name only
						$bankDomainPartial = str_ireplace(
							array("-", "BANK"),
							array("", ".?(?:e-|WEB ?)?BANK"),
							$bankDomainNoEnd
						);

						// Test for partial name with space
						if(
							preg_match('/' . $bankDomainPartial . '/i', $email->headerSubjectDecoded) === 1 &&
							stripos($email->envelopeFromDomain, $bankDomain) === false
						) {

							return array(
								'result' => "fail",
								'reason' => "Fake Bank #4 (" . $bankDomain . ")"
							);

						}

					}

					// Test for generic strings in Subject
					if(
						stripos($email->headerSubjectDecoded, "anaγkaia drash") !== false ||
						stripos($email->headerSubjectDecoded, "Exete nea eidopoihsh") !== false ||
						stripos($email->headerSubjectDecoded, "teleitaia ipenthimish") !== false
					) {

						return array(
							'result' => "fail",
							'reason' => "Fake Bank #5 (generic bank)"
						);

					}

				}

			}

			// Check for fake AlphaBank
			if(
				$country === "GR" &&
				stripos($email->headerFromNameDecoded, "myAlpha") !== false &&
				stripos($email->headerSubjectDecoded, "Alpha") !== false &&
				(
					stripos($email->headerFromDomain, "alphabank.gr") === false ||
					stripos($email->headerFromDomain, "alpha.gr") === false ||
					stripos($email->headerFromDomain, "alphaecommerce.gr") === false
				)
			)
				return array(
					'result' => "fail",
					'reason' => "Fake Bank (alpha.gr)"
				);

			// Check for fake ETE
			if(
				$country === "GR" &&
				(
					stripos($email->headerFromNameDecoded, "Ethnikh") !== false ||
					stripos($email->headerFromNameDecoded, "Trapez") !== false ||
					stripos($email->headerFromNameDecoded, "ETE") !== false ||
					stripos($email->headerFromNameDecoded, "NBG") !== false
				) &&
				(
					stripos($email->headerSubjectDecoded, "Drash") !== false ||
					stripos($email->headerSubjectDecoded, "enhmerosh") !== false ||
					stripos($email->headerSubjectDecoded, "ETE") !== false ||
					stripos($email->headerSubjectDecoded, "NBG") !== false ||
					stripos($email->headerSubjectDecoded, "Epikirosh") !== false ||
					stripos($email->headerSubjectDecoded, "apaiteitai") !== false ||
					stripos($email->headerSubjectDecoded, "Enerγopoihsh") !== false ||
					stripos($email->headerSubjectDecoded, "ipopth") !== false ||
					stripos($email->headerSubjectDecoded, "drasthriothta") !== false
				) &&
				stripos($email->headerFromDomain, "nbg.gr") === false
			)
				return array(
					'result' => "fail",
					'reason' => "Fake Bank (ETE)"
				);

		}

		return array(
			'result' => "pass",
			'reason' => ""
		);

	}

	/**
	 * Check for fake MAERSK
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeMaersk(Email &$email): array
	{

		if(
			(
				stripos($email->headerFromNameDecoded, "MAERSK") !== false ||
				stripos($email->headerFromNameDecoded, "export") !== false
			) &&
			stripos($email->headerSubjectDecoded, "MAERSK") !== false &&
			stripos($email->headerFromDomain, "maersk.com") === false
		)
			return array(
				'result' => "fail",
				'reason' => "Fake MAERSK"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake LinkedIn
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeLinkedIn(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "LinkedIn") !== false &&
			stripos($email->headerSubjectDecoded, "LinkedIn") !== false &&
			stripos($email->headerFromDomain, "linkedin.com") === false &&
			(
				!isset($email->headers['MESSAGE-ID']) ||
				stripos($email->headers['MESSAGE-ID'], "linkedin.com") === false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake LinkedIn"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake Louis Vuitton
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeLouisVuitton(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "Louis Vuitton") !== false &&
			stripos($email->headerSubjectDecoded, "Louis Vuitton") !== false &&
			stripos($email->headerFromDomain, "louisvuitton.com") === false &&
			(
				!isset($email->headers['MESSAGE-ID']) ||
				stripos($email->headers['MESSAGE-ID'], "louisvuitton.com") === false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake Louis Vuitton"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake DHL
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeDhl(Email &$email): array
	{

		if(
			(
				empty($email->headerFromNameDecoded) ||
				stripos($email->headerFromNameDecoded, "DHL") !== false
			) &&
			(
				stripos($email->headerSubjectDecoded, "action") !== false ||
				stripos($email->headerSubjectDecoded, "required") !== false ||
				stripos($email->headerSubjectDecoded, "confirm") !== false ||
				stripos($email->headerSubjectDecoded, "shipment") !== false ||
				stripos($email->headerSubjectDecoded, "dema") !== false
			) &&
			stripos($email->headerFromDomain, "dhl.com") === false &&
			(
				!isset($email->headers['MESSAGE-ID']) ||
				stripos($email->headers['MESSAGE-ID'], "dhl.com") === false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake DHL"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake USPS
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeUsps(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "USPS") !== false &&
			(
				stripos($email->headerSubjectDecoded, "Package notice") !== false ||
				stripos($email->headerSubjectDecoded, "Delivery Failure Notification") !== false ||
				stripos($email->headerSubjectDecoded, "Action Required") !== false ||
				stripos($email->headerSubjectDecoded, "Delivery Issue") !== false
			) &&
			stripos($email->headerFromDomain, "usps.com") === false &&
			(
				!isset($email->headers['MESSAGE-ID']) ||
				stripos($email->headers['MESSAGE-ID'], "usps.com") === false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake USPS"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake Chanel
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeChanel(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "chanel") !== false &&
			stripos($email->headerSubjectDecoded, "chanel") !== false &&
			(
				!isset($email->headers['MESSAGE-ID']) ||
				stripos($email->headers['MESSAGE-ID'], "chanel.com") === false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake Chanel"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake Elta
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeElta(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "ELTA") !== false &&
			preg_match('/elta\b/i', $email->headerFromDomain) !== 1 &&
			(
				stripos($email->headerSubjectDecoded, "paketo") !== false ||
				stripos($email->headerSubjectDecoded, "paradosh") !== false ||
				preg_match('/\d+GR/i', $email->headerSubjectDecoded) === 1
			) && (
				!isset($email->headers['MESSAGE-ID']) ||
				stripos($email->headers['MESSAGE-ID'], "elta") === false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake Elta"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake Facebook
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeFacebook(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "META") !== false &&
			stripos($email->headerFromNameDecoded, "BUSINESS") !== false &&
			stripos($email->headerSubjectDecoded, "facebook") !== false &&
			stripos($email->headerFromDomain, "facebook.com") === false &&
			isset($email->headers['X-SFDC-USER'])
		)
			return array(
				'result' => "fail",
				'reason' => "Fake Facebook"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake password expiration
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakePassword(Email &$email): array
	{

		if(
			(
				stripos($email->headerFromNameDecoded, "Email Support Team") !== false ||
				stripos($email->headerFromNameDecoded, "HelpDesk Support") !== false ||
				stripos($email->headerFromNameDecoded, "Password Manager") !== false ||
				stripos($email->headerFromNameDecoded, "Webmail Administrator") !== false ||
				stripos($email->headerFromNameDecoded, "IT-support") !== false ||
				stripos($email->headerFromNameDecoded, "E-mail Security") !== false ||
				stripos($email->headerFromNameDecoded, "Mail Delivery System") !== false ||
				stripos($email->headerFromNameDecoded, "MyG0V") !== false ||
				stripos($email->headerFromNameDecoded, "Office post") !== false ||
				stripos($email->headerFromNameDecoded, "Mail Administrator Notice") !== false ||
				stripos($email->headerFromNameDecoded, "Webmail") !== false ||
				stripos($email->headerFromNameDecoded, "Postmaster") !== false ||
				stripos($email->headerFromNameDecoded, " ICT") !== false
			) && (
				stripos($email->headerSubjectDecoded, "Webmail Password Expires") !== false ||
				stripos($email->headerSubjectDecoded, "Your Mailbox has expired") !== false ||
				stripos($email->headerSubjectDecoded, "Expiration Notification") !== false ||
				stripos($email->headerSubjectDecoded, "Password Expiration") !== false ||
				stripos($email->headerSubjectDecoded, "expires soon from") !== false ||
				stripos($email->headerSubjectDecoded, "Your Email Will Block") !== false ||
				stripos($email->headerSubjectDecoded, "Outgoing mail failed") !== false ||
				stripos($email->headerSubjectDecoded, "Yоur Aссоunt has been lосked") !== false ||
				stripos($email->headerSubjectDecoded, "important notify") !== false ||
				stripos($email->headerSubjectDecoded, "The mail quota has reached its maximum") !== false ||
				stripos($email->headerSubjectDecoded, "Password Expiry Notification") !== false ||
				stripos($email->headerSubjectDecoded, "You Have Important Email Notification") !== false ||
				stripos($email->headerSubjectDecoded, "kodikoi prosbashs") !== false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake Password"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake Netflix
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeNetflix(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "Netflix") !== false &&
			stripos($email->headerFromDomain, "netflix.com") === false
		)
			return array(
				'result' => "fail",
				'reason' => "Fake Netflix"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake Apple
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakeApple(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "Apple") !== false &&
			stripos($email->headerFromDomain, ".apple.com") === false &&
			(
				stripos($email->headerSubjectDecoded, "Important notice") !== false ||
				stripos($email->headerSubjectDecoded, "enhance security") !== false ||
				stripos($email->headerSubjectDecoded, "Apple Pay service has been suspended") !== false
			)
		)
			return array(
				'result' => "fail",
				'reason' => "Fake Apple"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * Check for fake PayPal
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function fakePaypal(Email &$email): array
	{

		if(
			stripos($email->headerFromNameDecoded, "PayPal") !== false &&
			stripos($email->headerFromDomain, "paypal.com") === false
		)
			return array(
				'result' => "fail",
				'reason' => "Fake PayPal"
			);
		else
			return array(
				'result' => "pass",
				'reason' => ""
			);

	}

	/**
	 * AbuseIPDB integration
	 * @param Email &$email
	 * @return array{result: string, reason: string}
	 */
	public static function abuseIpDb(Email &$email): array
	{

		// Do not proceed if there is no AbuseIPDB key
		$abuseIpdbKey = Config::readStr("abuseIpDbKey");
		if(empty($abuseIpdbKey))
			return array(
					'result' => "pass",
					'reason' => ""
				);

		// Get data from AbuseIPDB - return pass if AbuseIPDB failed to return proper data
		$data = Utils::abuseIpDb($email->connection['address'], $abuseIpdbKey);
		if(
			!is_object($data) ||
			!property_exists($data, "abuseConfidenceScore") ||
			!property_exists($data, "countryCode") ||
			!property_exists($data, "isWhitelisted") ||
			!property_exists($data, "isPublic")
		)
			return array(
					'result' => "pass",
					'reason' => ""
				);

		// Return pass on private or whitelisted IP addresses
		if(
			$data->isPublic === false ||
			$data->isWhitelisted === true
		)
			return array(
					'result' => "pass",
					'reason' => ""
				);

		// Test for the confidence score
		$blockOnConfidenceScore = intval(Config::read("abuseIpDbBlockOnConfidenceScore"));
		if(intval($data->abuseConfidenceScore) >= $blockOnConfidenceScore)
			return array(
				'result' => "fail",
				'reason' => "AbuseIPDB confidence score too high (" . $data->abuseConfidenceScore . " >= " . $blockOnConfidenceScore . ")"
			);

		// Test for blacklisted countries
		$blacklistCountries = Config::read("abuseIpDbBlacklistCountries");
		if(
			is_array($blacklistCountries) &&
			in_array($data->countryCode, $blacklistCountries)
		)
			return array(
				'result' => "fail",
				'reason' => "AbuseIPDB blacklisted country (" . $data->countryCode . ")"
			);
		else
			return array(
					'result' => "pass",
					'reason' => ""
				);

	}

}

