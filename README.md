# guardian-milter

```
                           _ _                             _ _ _
  __ _ _   _  __ _ _ __ __| (_) __ _ _ __        _ __ ___ (_) | |_ ___ _ __
 / _` | | | |/ _` | '__/ _` | |/ _` | '_ \ _____| '_ ` _ \| | | __/ _ \ '__|
| (_| | |_| | (_| | | | (_| | | (_| | | | |_____| | | | | | | | ||  __/ |
 \__, |\__,_|\__,_|_|  \__,_|_|\__,_|_| |_|     |_| |_| |_|_|_|\__\___|_|
 |___/

```

Multi-purpose security milter.

Guardian-milter is a Postfix/Sendmail milter for filtering various types of fake, phishing and spam emails. The milter executes highly crafted checks and marks incoming emails with a pass/fail header or rejects them during the SMTP dialogue.

# Features

- Add an Authentication-Results header
- Copy failed emails to another email address
- Reject failed emails during the SMTP conversation
- Remove the name part of the From: header
- Save failed emails in JSON format
- Run regular expressions on the connection (IP/hostname)
- Run regular expressions on the HELO/EHLO string
- Run regular expressions on the envelope RCTP
- Detect domain in the From name
- Detect domain in the subject
- Detect fake Apple emails
- Detect fake banking emails
- Detect fake Chanel emails
- Detect fake DHL emails
- Detect fake ELTA emails
- Detect fake Facebook emails
- Detect fake LinkedIn emails
- Detect fake Louis Vuitton emails
- Detect fake Maersk emails
- Detect fake password expiration emails
- Detect fake PayPal emails
- Detect fake USPS emails
- Detect fake WeTransfer emails
- Reject attachments with known problematic file extensions
- AbuseIPDB integration
- Block emails based on IP confidence score (AbuseIPDB)
- Block emails by country of origin (AbuseIPDB)

# Requirements

- [libMilterPHP](https://gitlab.com/noumenia/libmilterphp)
- PHP 8.0, 8.1, 8.2, 8.3
- iconv module
- posix module
- sockets module


# Install with RPM packages

You may install guardian-milter via the copr repository, for Alma/Rocky/Oracle Enterprise Linux and Fedora, simply use:

```
dnf copr enable mksanthi/noumenia
dnf install guardian-milter
```


# Install with Composer

You may install guardian-milter with composer, to get the latest version use the create-project command, the last dot is important because it tells composer to save the files in the current directory.

```
composer create-project noumenia/guardian-milter .
```


# Configure postfix to use guardian-milter

Edit the `/etc/postfix/main.cf` file, add or modify the existing `smtpd_milters` parameter with the connection string for guardian-milter. For example:

```
smtpd_milters = inet:127.0.0.1:8895
```


# How to use

If you installed via the RPM packages then a systemd service is already available and guardian-milter can be started with systemctl. By default, guardian-milter will read the configuration file `/etc/gu-milter.ini` and listen for connections at `127.0.0.1` port `8895`.

```
systemctl --now enable guardian-milter
```

If you installed via composer or manually, then the `gu-milter.ini` file will be located in the same directory as the guardian-milter executable and can be specified with the `--ini=...` parameter. Below is the complete list of command-line parameters:

```
Usage: guardian-milter [OPTION]...

  -V,  --version             display version information only
  -h,  --help                display help about parameters
  -v,  --verbose             enable verbose output to stdout

  -u,  --user=[USER]         Effective user
  -g,  --group=[GROUP]       Effective group
  -p,  --pid=[FILE]          PID file
  -l,  --processlimit=[NUM]  Process limit
  -c,  --connection=[STRING] Connection string
  -a,  --autoload=[FILE]     LibMilterPHP autoloader (controller/common.inc.php)
  -i,  --ini=[FILE]          Configuration file (/etc/gu-milter.ini)

       --addHeader           Add Authentication-Results header with pass/fail information
       --reject              Reject failed emails
       --removeName          Remove From: header name part

 The connection string can be a UNIX socket or an IPv4/IPv6 address/port.
 UNIX Socket...: unix:/run/gu-milter/guardian-milter.sock
 Address/port..: inet:8895@127.0.0.1

```


# Related projects

- [Aetolos -  Virtual hosting at your... command line!](https://gitlab.com/noumenia/aetolos)
- [libMilterPHP is a Postfix/Sendmail Milter library implementation in PHP.](https://gitlab.com/noumenia/libmilterphp)
- [mmDbDaemon is a memory-resident MaxMind Database reader implementation in PHP.](https://gitlab.com/noumenia/mmdbdaemon)

