# Changelog guardian-milter

## Development

## Release 2.0

- Improve fake subject tests
- Transliterate Greek s letter
- Add check for fake PayPal emails
- Improve fake ETE tests
- Improve strict type checking
- Improve fake ELTA tests
- Improve fake DHL tests
- Add check if the recipient name is within the Subject
- Improve recipient domain matching fake notifications
- Case insensitive regular expression
- Transliterate Greek x letter
- Add check for fake Apple emails
- Replace broken Apple Pay strings
- Transliterate Cyrillic letters
- Improve fake Bank tests
- Input validation for empty URL
- Imrovements for strict type checking
- Transliterate Cherokee, Coptic and Armenian letters

## Release 1.9

- Improve fake alphabank tests
- Add check for fake Netflix emails
- Improve fake password tests
- Improve fake Bank tests

## Release 1.8

- AbuseIPDB integration
- Reject emails by IP confidence score
- Reject emails by IP country blacklist
- Remove user input that causes regular expression errors
- Improve fake ETE tests
- Improve fake Bank tests
- Transliterate more characters
- Remove various characters that are used to trick text matching
- Improve fake ELTA tests

## Release 1.7

- Add check for fake password expiration emails
- Add check for fake Facebook emails
- Blacklist and reject emails by file extension
- Parse first level MIME file attachments
- Set a default extension blacklist
- Allow body content to be sent to the milter
- Update the default ini file and enable all tests
- Regular expression tests for envelope RCPT
- Regular expression tests for connection IP/hostname
- Add a numeric index to the bank tests
- The address key of the connection is not optional
- More aggressive wetransfer test

## Release 1.6

- Report input validation errors
- Collect and generate multiple errors
- The reject option affects input validation errors
- Report PID errors via Log::error
- Use a common saveToJson() function
- Do not reject early if reject ini option is false
- Create the PID directory before dropping privileges
- Set the user/group owner of the PID directory
- Properly define the runtime /run/gu-milter directory

## Release 1.5

- Store the queue ID and use it as a log prefix
- Log PASS when an email passes all checks
- Test for more decoded name strings in fakeBank
- Transliterate more characters
- Documentation changes
- Improve fake DHL check

## Release 1.4

- Improve fakeBank check
- Add check for fake ELTA emails
- Update HELO/EHLO blacklist
- Avoid false positives with domain renewal subjects
- Run iconv_mime_decode on encoded strings only
- Transliterate more characters
- Add result/reason to the Email object
- Force reject with SMFIR_REPLYCODE
- Only generate a single error per rejection
- Add check for fake Chanel emails
- Handle cases when the Message-ID is missing
- Improve strict type checking
- Save errors to the /run/gu-milter directory
- Validate the PID directory
- Handle errors while changing to the PID directory

## Release 1.3

- Add check for fake USPS emails
- Keep the connection information on SMFIC_ABORT
- Add check for fake ETE emails
- Transliterate Greek characters
- Improve detection of BANK strings
- Log to LOG_MAIL instead of LOG_DAEMON
- Implement a common transliterate function
- Add check for fake DHL emails
- Add check for fake Louis Vuitton emails
- Add check for fake LinkedIn emails
- Add check for fake MAERSK emails

## Release 1.2

- Improve the loading of the help controller
- Reset Email object on SMFIC_ABORT
- Set current working directory the same as the PID file
- Remove redundant condition checks
- Enforce type checks
- Set default port number to 8895
- Failed emails are rejected before any modifications

## Release 1.1

- Copy failed emails to a specified email address
- Make /etc the default location for gu-milter.ini
- Listen by default at 127.0.0.1 port 8895
- Allow guardian-milter to run without any parameters
- Implement the removeName command-line parameter
- Remove the From: name part after all checks have run
- Decode Cyrillic characters that obfuscate the word BANK
- Update project description
- Place run files under our own /run/gu-milter directory

## Release 1.0

- Generate RPM packages
- Update composer.json
- Initial public release

